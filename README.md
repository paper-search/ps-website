# Paper Search Website

![CI](https://gitlab.com/paper-search/ps-website/badges/master/pipeline.svg)

![paper search website screenshot](screenshot.png "Paper Search website")

## Information
This website uses [jekyll](https://jekyllrb.com/) to generate a static website.
It is based on the Jekyll Hydra Theme.

## Usage
This project is deployed using *GitLab CI/CD*, but it can also be run locally:
```shell
git clone <repository url>
cd ps-website
./fetch_issues.sh # we need this script to fetch the issues list for the issue tracker
./update-issue-labels.sh # this is optional. It fetches the colors of the issue labels, but requires you to add a token inside update-issue-labels.sh
bundle exec jekyll serve
```

## Dependencies
This project requires [jekyll](https://jekyllrb.com/), which itself requires [ruby](https://www.ruby-lang.org/)

## TODO Notes before release

### Server that handles contact page POST requests
The contact page sends the E-Mail via a HTTP POST request. It can currently be found here:
[contact.html line 13](https://gitlab.com/paper-search/ps-website/-/blob/master/contact.html#L13)
The POST-request currently points to ```http://paper-search.com/post-req-handler```. This URL should be swapped to the receiving server of the POST-request, since Jekyll is a static site generator and can not accept POST requests.
The server should then redirect to ```contact-success.html```, as specified in the next line below the url: ```<input type='hidden' name='redirect_to' value=<contact success page url>```
### Issues links
Currently, the curl command in ```fetch_issues.sh``` fetches gnome's issues rather than paper-search's issues due to authentification problems since the repository is private.
This link should be changed as soon as the project's repository goes public.
### Issue labels
run ```update-issue-labels.sh``` manually (see the file ```update-issue-labels.sh``` itself's comments for more details).
Currently, gnome's issue label colors are used. We have to replace this file manually in order to use Paper Search label colors

### (Update margins after updating text in the features-list)
The features list should be able to handle the text accordingly,
but if something looks bad formatted or the bulletpoints do not have enough space between them, 
the margin might have to be changed. This can be done by modifying the CSS 'margin-bottom' attribute of the wine-row-left and wine-row-right class.
The corresponding code can be found here:
[Left row (first 3 bulletpoints) mobile](https://gitlab.com/paper-search/ps-website/-/blob/master/_sass/layout.scss#L267)
[Left row (first 3 bulletpoints)](https://gitlab.com/paper-search/ps-website/-/blob/master/_sass/layout.scss#L274)
[Right row (last 3 bulletpoints) mobile](https://gitlab.com/paper-search/ps-website/-/blob/master/_sass/layout.scss#L286)
[Right row (last 3 bulletpoints)](https://gitlab.com/paper-search/ps-website/-/blob/master/_sass/layout.scss#L293)
