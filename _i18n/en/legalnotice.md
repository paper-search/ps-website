Information according to § 5 TMG

Paper Search
Apianstraße 7
85774 Unterföhring

### Represented by
Fabian Sauter

### Contact
e-mail: contact@paper-search.com

### Disclaimer

#### Liability for links

Our offer contains links to external websites of third parties, on whose contents we have no influence. Therefore we cannot assume any liability for these external contents. The respective provider or operator of the sites is always responsible for the contents of the linked sites. The linked pages were checked for possible legal violations at the time of linking. Illegal contents were not identified at the time of linking. However, a permanent control of the contents of the linked pages is not reasonable without concrete evidence of a violation of the law. If we become aware of any infringements, we will remove such links immediately.

#### Privacy policy

The use of our website is usually possible without providing personal data. As far as personal data (e.g. name, address or e-mail addresses) is collected on our website, this is always done on a voluntary basis, as far as possible. This data will not be passed on to third parties without your express consent.
We would like to point out that data transmission over the Internet (e.g. communication by e-mail or contact form) can have security gaps. A complete protection of data against access by third parties is not possible.
The use of contact data published in the context of the imprint obligation by third parties for sending advertising and information material not expressly requested is hereby expressly prohibited. The operators of the site expressly reserve the right to take legal action in the event that unsolicited advertising information is sent, for example by spam mail.
