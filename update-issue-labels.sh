# This file should be executed manually(!) to update the issue labels (including its
# background colors and text colors), which are needed for rendering the development page issue tracker.
# The downloaded data will be stored in _data/api_fetch/issue_labels.json
# and used in development.html (jekyll liquid)

# This can not be automated since it always needs an access token, even if the repository is public.
# The access token <access_token> needs to be replaced in the script and can be generated in the settings
# of GitLab of a member of the paper search main project
# This script should always be executed after adding a new issue label, and the resulting
# file _data/api_fetch/issue_labels.json should be pushed to the repository

curl --header "PRIVATE-TOKEN: <access_token>" https://gitlab.com/api/v4/projects/11555965/labels?with_counts=true > _data/api_fetch/issue_labels.json
