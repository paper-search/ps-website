# This script gets executed automatically by GitLab CI/CD as specified in .gitlab-ci.yml
# It fetches all data needed for the issue tracker into the directory  /_data/api_fetch/
# The resulting files are /_data/api_fetch/issues_opened.json and /_data/api_fetch/issues_stats.json
# They are then rendered in development.html using jekyll liquid

# The lines starting with curl --header [...] are for fetching our paper search files
# The lines starting with curl -o- [...] are for fetching gnome's files for mocking purposes
# Once the repository is public, we should only use paper search curls and remove the others

echo 'creating directory for storing issues fetch data'
mkdir _data/api_fetch
echo 'fetching issue-statistics ..'
#curl --header "PRIVATE-TOKEN: <TOKEN>" https://gitlab.com/api/v4/projects/11555965/issues_statistics > ./_data/api_fetch/issues_stats.json
curl -o- https://gitlab.gnome.org/api/v4/projects/665/issues_statistics > ./_data/api_fetch/issues_stats.json
echo 'successfully fetched issue-statistics'
echo 'fetching issues'
#curl --header "PRIVATE-TOKEN: <TOKEN>" https://gitlab.com/api/v4/projects/11555965/issues > ./_data/api_fetch/issues_opened.json
curl -o- https://gitlab.gnome.org/api/v4/projects/665/issues?state=opened > ./_data/api_fetch/issues_opened.json
echo 'successfully fetched issues'
