/*
 * Main JavaScript file, implementing the following features:
 *
 * - tooltip feature:
 *      Shows a specialized tooltip when hovering over
 *      different areas of the images
 *
 * - hitbox highlighting feature:
 *      Creates a blue blinking effect over the tooltip
 *      areas and stops it when the mouse enters an image.
 *      This should indicate, which areas of the images can be hovered on
 *
 * Also refer to _sass/layout.scss for the corresponding css and index.html
 * for the corresponding html (especially for the images)
 */

/* Mouse coordinates, updated in function mousemove */
var mouseX;
var mouseY;

/* Mouse coordinates inside the image, updated in function mousemove */
var mouseXonImg;
var mouseYonImg;

/* width and height of the current image */
var imgWidth;
var imgHeight

/* boolean indicating whether the mouse is on an image */
var mouseOnImg = false;

/* boolean indicating if the mouse is on a highlighted part of the image */
var mouseOnHighlight = false;

/* integer indicating which image the user is hovering on */
var selectedImg = -1;

/* This is set to true if the last touch was on the highlightbox rather than
 * the image itself */
var lastHighlightTouchWasMobile = false;

/* Highlighting Boxes for Image 1 */
var IMG1_BOX1_EDGE_TOPLEFT_X = 0.11;
var IMG1_BOX1_EDGE_TOPLEFT_Y = 0.17;
var IMG1_BOX1_EDGE_BOTTOMRIGHT_X = 0.84;
var IMG1_BOX1_EDGE_BOTTOMRIGHT_Y = 0.28;

var IMG1_BOX2_EDGE_TOPLEFT_X = 0.0;
var IMG1_BOX2_EDGE_TOPLEFT_Y = 0.30;
var IMG1_BOX2_EDGE_BOTTOMRIGHT_X = 0.988;
var IMG1_BOX2_EDGE_BOTTOMRIGHT_Y = 0.91;

/* Highlighting Boxes for Image 2 */
var IMG2_BOX1_EDGE_TOPLEFT_X = 0.013;
var IMG2_BOX1_EDGE_TOPLEFT_Y = 0.148;
var IMG2_BOX1_EDGE_BOTTOMRIGHT_X = 0.978;
var IMG2_BOX1_EDGE_BOTTOMRIGHT_Y = 0.978;

/* Highlighting Boxes for Image 3 */
var IMG3_BOX1_EDGE_TOPLEFT_X = 0.005;
var IMG3_BOX1_EDGE_TOPLEFT_Y = 0.15;
var IMG3_BOX1_EDGE_BOTTOMRIGHT_X = 0.495;
var IMG3_BOX1_EDGE_BOTTOMRIGHT_Y = 0.305;

var IMG3_BOX2_EDGE_TOPLEFT_X = 0.005;
var IMG3_BOX2_EDGE_TOPLEFT_Y = 0.502;
var IMG3_BOX2_EDGE_BOTTOMRIGHT_X = 0.985;
var IMG3_BOX2_EDGE_BOTTOMRIGHT_Y = 0.875;

var IMG3_BOX3_EDGE_TOPLEFT_X = 0.005;
var IMG3_BOX3_EDGE_TOPLEFT_Y = 0.88;
var IMG3_BOX3_EDGE_BOTTOMRIGHT_X = 0.985;
var IMG3_BOX3_EDGE_BOTTOMRIGHT_Y = 0.965;

/* Texts of the Highlight boxes are not in separate file
 * highlight_texts_de and highlight_texts_en */

/*
 * Needed to update the mouse X and Y values, in order to show the tooltip
 * correctly
 */
$(document).mousemove( function(event) {
   // mouse coordinates
   mouseX = event.pageX;
   mouseY = event.pageY;

   //mouseXonImg = event.offsetX ? (event.offsetX) : event.pageX - img.offsetLeft;
   mouseXonImg = event.offsetX ? (event.offsetX) : 1;
   //mouseYonImg = event.offsetY ? (event.offsetY) : event.pageY - img.offsetTop;
   mouseYonImg = event.offsetY ? (event.offsetY) : 1;

   var ratioX = (mouseXonImg/imgWidth);
   var ratioY = (mouseYonImg/imgHeight);
   //console.log((mouseXonImg/imgWidth) + "/" + (mouseYonImg/imgHeight));
   mouseOnHighlight = false;

   switch(selectedImg) {
     case 1:
       if(ratioY >= IMG1_BOX1_EDGE_TOPLEFT_Y && ratioY <= IMG1_BOX1_EDGE_BOTTOMRIGHT_Y) {
         mouseOnHighlight = true;
         $('#tooltip span').html(IMG1_BOX1_TEXT);
       } else if(ratioY >= IMG1_BOX2_EDGE_TOPLEFT_Y && ratioY <= IMG1_BOX2_EDGE_BOTTOMRIGHT_Y && ratioX >= IMG1_BOX2_EDGE_TOPLEFT_X && ratioX <= IMG1_BOX2_EDGE_BOTTOMRIGHT_X) {
         mouseOnHighlight = true;
         $('#tooltip span').html(IMG1_BOX2_TEXT);
       } else {
         if(mouseOnHighlight) {
           mouseOnHighlight = false;
           $('#tooltip').stop(false, true).fadeOut('fast');
         }
       }
       break;
    case 2:
      if(ratioX >= IMG2_BOX1_EDGE_TOPLEFT_X && ratioX <= IMG2_BOX1_EDGE_BOTTOMRIGHT_X && ratioY >= IMG2_BOX1_EDGE_TOPLEFT_Y && ratioY <= IMG2_BOX1_EDGE_BOTTOMRIGHT_Y) {
        mouseOnHighlight = true;
        $('#tooltip span').html(IMG2_BOX1_TEXT);
      } else {
        if(mouseOnHighlight) {
          mouseOnHighlight = false;
          $('#tooltip').stop(false, true).fadeOut('fast');
        }
      }
      break;
    case 3:
      if(ratioX >= IMG3_BOX1_EDGE_TOPLEFT_X && ratioX <= IMG3_BOX1_EDGE_BOTTOMRIGHT_X && ratioY >= IMG3_BOX1_EDGE_TOPLEFT_Y && ratioY <= IMG3_BOX1_EDGE_BOTTOMRIGHT_Y) {
        $('#tooltip span').html(IMG3_BOX1_TEXT);
      } else if(ratioY >= IMG3_BOX2_EDGE_TOPLEFT_Y && ratioY <= IMG3_BOX2_EDGE_BOTTOMRIGHT_Y) {
        $('#tooltip span').html(IMG3_BOX2_TEXT);
      } else if(ratioY > 0.92) {
        $('#tooltip span').html(IMG3_BOX3_TEXT);
      } else {
        if(mouseOnHighlight) {
          mouseOnHighlight = false;
          $('#tooltip').stop(false, true).fadeOut('fast');
        }
      }
      break;
   }




});

/*
 * Called to stop the highlight box blinking and start showing
 */
function mouseOver(event, img) {

  mouseOnImg = true;

  imgWidth = img.width;
  imgHeight = img.height;

  if(img.id==="img1") {
    selectedImg = 1;
  } else if(img.id==="img2") {
    selectedImg = 2;
  } else if(img.id==="img3") {
    selectedImg = 3;
  }
  // populate tooltip string
  //$('#tooltip span').html((mouseXonImg/imgWidth) + "/" + (mouseYonImg/imgHeight));

  // remove highlight boxes to avoid conflicts with the tooltip
  $(".img1box1").stop(true,true).fadeOut('fast');
  $(".img1box2").stop(true,true).fadeOut('fast');
  $(".img2box1").stop(true,true).fadeOut('fast');
  $(".img3box1").stop(true,true).fadeOut('fast');
  $(".img3box2").stop(true,true).fadeOut('fast');
  $(".img3box3").stop(true,true).fadeOut('fast');
  // show tooltip
  $('#tooltip').stop(false, true).fadeIn(1);

  // position tooltip relative to mouse coordinates
  $(this).mousemove(function() {
    var newX = mouseX - 180 < 0 ? 0 : mouseX - 180;
    var newY = mouseY - 180;
    $('#tooltip').css({'top':newY,'left':newX});
  });
}

/*
 * Function which stops the tooltip as soon as the mouse leaves the image
 */
function mouseOut(event, img) {
  // hide tooltip
  mouseOnImg = false;
  $('#tooltip').stop(false, true).fadeOut('fast');
}

// This might be needed in the future for further mobile improvements
function onClicked() {

}

/*
 * This function updates the highlight hitboxes via jQuery and CSS,
 * and is called by onresize and onload
 */
updateHighlightBoxes = function() {
  var img1width = $("#img1").width();
  var img1height = $("#img1").height();
  var img2width = $("#img2").width();
  var img2height = $("#img2").height();
  var img3width = $("#img3").width();
  var img3height = $("#img3").height();

  // update highlight boxes for Image 1
  $(".img1box1").css("top", (img1height * IMG1_BOX1_EDGE_TOPLEFT_Y));
  $(".img1box1").css("left", (img1width * IMG1_BOX1_EDGE_TOPLEFT_X));
  $(".img1box1").css("width", (img1width * (IMG1_BOX1_EDGE_BOTTOMRIGHT_X - IMG1_BOX1_EDGE_TOPLEFT_X)));
  $(".img1box1").css("height", (img1height * (IMG1_BOX1_EDGE_BOTTOMRIGHT_Y - IMG1_BOX1_EDGE_TOPLEFT_Y)));

  $(".img1box2").css("top", (img1height * IMG1_BOX2_EDGE_TOPLEFT_Y));
  $(".img1box2").css("left", (img1width * IMG1_BOX2_EDGE_TOPLEFT_X));
  $(".img1box2").css("width", (img1width * (IMG1_BOX2_EDGE_BOTTOMRIGHT_X - IMG1_BOX2_EDGE_TOPLEFT_X)));
  $(".img1box2").css("height", (img1height * (IMG1_BOX2_EDGE_BOTTOMRIGHT_Y - IMG1_BOX2_EDGE_TOPLEFT_Y)));

  // update highlight box for Image 2
  $(".img2box1").css("top", (img2height * IMG2_BOX1_EDGE_TOPLEFT_Y));
  $(".img2box1").css("left", (img2width * IMG2_BOX1_EDGE_TOPLEFT_X));
  $(".img2box1").css("width", (img2width * (IMG2_BOX1_EDGE_BOTTOMRIGHT_X - IMG2_BOX1_EDGE_TOPLEFT_X)));
  $(".img2box1").css("height", (img2height * (IMG2_BOX1_EDGE_BOTTOMRIGHT_Y - IMG2_BOX1_EDGE_TOPLEFT_Y)));

  // update highlight boxes for Image 3
  $(".img3box1").css("top", (img3height * IMG3_BOX1_EDGE_TOPLEFT_Y));
  $(".img3box1").css("left", (img3width * IMG3_BOX1_EDGE_TOPLEFT_X));
  $(".img3box1").css("width", (img3width * (IMG3_BOX1_EDGE_BOTTOMRIGHT_X - IMG3_BOX1_EDGE_TOPLEFT_X)));
  $(".img3box1").css("height", (img3height * (IMG3_BOX1_EDGE_BOTTOMRIGHT_Y - IMG3_BOX1_EDGE_TOPLEFT_Y)));

  $(".img3box2").css("top", (img3height * IMG3_BOX2_EDGE_TOPLEFT_Y));
  $(".img3box2").css("left", (img3width * IMG3_BOX2_EDGE_TOPLEFT_X));
  $(".img3box2").css("width", (img3width * (IMG3_BOX2_EDGE_BOTTOMRIGHT_X - IMG3_BOX2_EDGE_TOPLEFT_X)));
  $(".img3box2").css("height", (img3height * (IMG3_BOX2_EDGE_BOTTOMRIGHT_Y - IMG3_BOX2_EDGE_TOPLEFT_Y)));

  $(".img3box3").css("top", (img3height * IMG3_BOX3_EDGE_TOPLEFT_Y));
  $(".img3box3").css("left", (img3width * IMG3_BOX3_EDGE_TOPLEFT_X));
  $(".img3box3").css("width", (img3width * (IMG3_BOX3_EDGE_BOTTOMRIGHT_X - IMG3_BOX3_EDGE_TOPLEFT_X)));
  $(".img3box3").css("height", (img3height * (IMG3_BOX3_EDGE_BOTTOMRIGHT_Y - IMG3_BOX3_EDGE_TOPLEFT_Y)));
};

/* Resize handler for blink effect. This is needed to place the
 * blue hitboxes properly and update their positions, because the image
 * can scale
 */
window.onresize = function() {
  updateHighlightBoxes();
};

/* When the website is loaded, two things have to be done for the
 * hitbox highlighting feature:
 * 1. update the highlight boxes (like in onresize)
 * 2. Start the interval, which creates the blink effect (fadeIn/fadeOut)
 */
window.onload = function() {
  updateHighlightBoxes();
  setInterval(function(){
        if(!mouseOnImg) {
          $(".img1box1").fadeIn(1000).fadeOut(1000);
          $(".img1box2").fadeIn(1000).fadeOut(1000);
          $(".img2box1").fadeIn(1000).fadeOut(1000);
          $(".img3box1").fadeIn(1000).fadeOut(1000);
          $(".img3box2").fadeIn(1000).fadeOut(1000);
          $(".img3box3").fadeIn(1000).fadeOut(1000);
        }
    }, 3000);
};

$(window).scroll(function() {
  if(lastHighlightTouchWasMobile) {
    mouseOnImg = false;
    $('#tooltip').stop(false, true).fadeOut('fast');
    lastHighlightTouchWasMobile = false;
  }
});

function clickOnImgBox(event, box) {

  mouseOnHighlight = true;
  mouseOnImg = true;
  lastHighlightTouchWasMobile = true;
  if(box.className === "img1box1") {
    $('#tooltip span').html(IMG1_BOX1_TEXT);
  } else if(box.className === "img1box2") {
    $('#tooltip span').html(IMG1_BOX2_TEXT);
  } else if(box.className === "img2box1") {
    $('#tooltip span').html(IMG2_BOX1_TEXT);
  } else if(box.className === "img3box1") {
    $('#tooltip span').html(IMG3_BOX1_TEXT);
  } else if(box.className === "img3box2") {
    $('#tooltip span').html(IMG3_BOX2_TEXT);
  } else if(box.className === "img3box3") {
    $('#tooltip span').html(IMG3_BOX3_TEXT);
  }
  var newX = event.pageX - 180 < 0 ? 0 : event.pageX - 180;
  var newY = event.pageY - 180;
  $('#tooltip').css({'top':newY,'left':newX});
  $('#tooltip').stop(false, true).fadeIn(1);
}
