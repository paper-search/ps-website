/*
 * Highlight box texts for language "de"
 *
 * This file contains the text, which is displayed when the user hovers over
 * a blue highlighted area of an image. It is outsourced from main.js to
 * this file because it is language-specific
 */

var IMG1_BOX1_TEXT = "<p><strong>Automatisches Highlighten</strong> basierend auf den gegebenen Suchbegriffen</p>";
var IMG1_BOX2_TEXT = "<p><strong>Anzeige von Titeln und<br>Abstracts</strong> inklusive der wichtigsten Ergebnisse</p>";
var IMG2_BOX1_TEXT = "<p><strong>Ein schneller Überblick:</strong><br> Ein Bild sagt mehr als 1000 Worte</p>";
var IMG3_BOX1_TEXT = "<p>Einfacher Zugriff auf <strong>Millionen von Fachartikeln</strong> aus verschiedenen Datenbanken</p>";
var IMG3_BOX2_TEXT = "<p><strong>Intuitive</strong> und <strong>komfortable</strong> Suchmaske</p>";
var IMG3_BOX3_TEXT = "<p>Mit <strong>nur einem Klick</strong> können hunderte Fachartikel <strong>geladen</strong> und <strong>automatisch formatiert</strong> werden.</p>";
