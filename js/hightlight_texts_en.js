/*
 * Highlight box texts for language "de"
 *
 * This file contains the text, which is displayed when the user hovers over
 * a blue highlighted area of an image. It is outsourced from main.js to
 * this file because it is language-specific
 */

 var IMG1_BOX1_TEXT = "<p><strong>Automatic Highlighting</strong> based on the given search terms</p>";
 var IMG1_BOX2_TEXT = "<p><strong>Display of titles and <br>abstracts</strong> including the most important results</p>";
 var IMG2_BOX1_TEXT = "<p><strong>A quick overview:</strong><br> A picture says more than 1000 words</p>";
 var IMG3_BOX1_TEXT = "<p>Easy access to <strong>millions of professional articles</strong> from various databases</p>";
 var IMG3_BOX2_TEXT = "<p><strong>Intuitive</strong> and <strong>comfortable</strong> search mask</p>";
 var IMG3_BOX3_TEXT = "<p>With <strong>just one click</strong> hundreds of professional articles can be <strong>loaded automatically</strong></p>";
